import HighchartsReact from "highcharts-react-official";
import Highcharts from "highcharts/highstock";
import React, { useEffect, useRef, useState } from "react";
import {
  ADJUSTMENTMETHOD,
  BASEURL,
  ENDTIME,
  FIELDS,
  IDENTIFIER,
  IDENTIFIERTYPE,
  INCLUDEEXTENDED,
  STARTTIME,
} from "../../consts/urlParams";
import { IChartData } from "../../interfaces/charts/chart";
import { convertDataForChart } from "../../utils/chartHelpers";
import { httpClient } from "../../utils/http";
import TimeStampTabs from "../TimeStampTabs/TimeStampTabs";

const Chart = () => {
  const [chartData, setChartData] = useState<IChartData[]>();
  const [periodAndPrecision, setPeriodAndPrecision] = useState({
    period: "1",
    precision: "minute",
  });
  const chartRef =
    useRef<{
      chart: Highcharts.Chart;
      container: React.RefObject<HTMLDivElement>;
    }>(null);

  const fetchData = async () => {
    chartRef.current?.chart.showLoading();
    const { data } = await httpClient.get(
      `${BASEURL}?Identifier=${IDENTIFIER}&IdentifierType=${IDENTIFIERTYPE}&AdjustmentMethod=${ADJUSTMENTMETHOD}&IncludeExtended=${INCLUDEEXTENDED}&period=${periodAndPrecision["period"]}&Precision=${periodAndPrecision["precision"]}&StartTime=${STARTTIME}&EndTime=${ENDTIME}&_fields=${FIELDS}`
    );
    setChartData(convertDataForChart(data));
    chartRef.current?.chart.hideLoading();
  };

  const options: Highcharts.Options = {
    title: {
      text: "My stock chart",
    },
    rangeSelector: {
      buttons: [],
    },
    series: [
      {
        type: "candlestick",
        name: "AAPL Stock Price",
        data: chartData,
        dataGrouping: {
          units: [
            [periodAndPrecision["precision"], [+periodAndPrecision["period"]]],
          ],
        },
      },
    ],
  };
  useEffect(() => {
    fetchData();
  }, [periodAndPrecision]);
  return (
    <>
      <HighchartsReact
        ref={chartRef}
        highcharts={Highcharts}
        constructorType={"stockChart"}
        options={options}
      />
      <TimeStampTabs setPeriodAndPercision={setPeriodAndPrecision} />
    </>
  );
};
export default Chart;
