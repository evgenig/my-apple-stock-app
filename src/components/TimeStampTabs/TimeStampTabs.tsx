import Paper from "@material-ui/core/Paper";
import Tab from "@material-ui/core/Tab";
import Tabs from "@material-ui/core/Tabs";
import React from "react";

interface ITimeStampTabsProps {
  setPeriodAndPercision: React.Dispatch<
    React.SetStateAction<{
      period: string;
      precision: string;
    }>
  >;
}
const TimeStampTabs = (props: ITimeStampTabsProps) => {
  const { setPeriodAndPercision } = { ...props };
  const [value, setValue] = React.useState(0);

  return (
    <Paper>
      <Tabs
        value={value}
        onChange={(_, newValue) => setValue(newValue)}
        indicatorColor="primary"
        textColor="primary"
        centered
      >
        <Tab
          label="1 minute"
          onClick={() =>
            setPeriodAndPercision({ period: "1", precision: "minute" })
          }
        />
        <Tab
          label="5 minute"
          onClick={() =>
            setPeriodAndPercision({ period: "5", precision: "minute" })
          }
        />
        <Tab
          label="1 hour"
          onClick={() =>
            setPeriodAndPercision({ period: "1", precision: "hour" })
          }
        />
        <Tab
          label="1 week"
          onClick={() =>
            setPeriodAndPercision({ period: "168", precision: "hour" })
          }
        />
      </Tabs>
    </Paper>
  );
};
export default TimeStampTabs;
