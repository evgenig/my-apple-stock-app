export interface IChartData {
  date: string;
  open: number;
  high: number;
  low: number;
  close: number;
}

export interface IDataResponse {
  StartDate: string;
  StartTime: string;
  Open: number;
  High: number;
  Low: number;
  Close: number;
  Date: string;
  Volume: number;
}
