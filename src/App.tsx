import React from "react";
import Chart from "./components/Charts/Chart";

function App() {
  return (
    <div className="App">
      <Chart />
    </div>
  );
}

export default App;
