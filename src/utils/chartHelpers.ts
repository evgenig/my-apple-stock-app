import { IDataResponse } from "../interfaces/charts/chart";
const toTimestamp = (
  year: string,
  month: string,
  day: string,
  hour: string,
  minute: string,
  second: string
) => {
  var datum = new Date(
    Date.UTC(+year, +month - 1, +day, +hour, +minute, +second)
  );
  return datum.getTime();
};
export const convertDataForChart = (data: IDataResponse[]): any => {
  let dataToSet: any = [];
  data.forEach((candle) => {
    let date = candle.Date.split(" ");
    let startDate = date[0].split("/");
    let startTime = date[1].split(":");
    let timeStamp = toTimestamp(
      startDate[0],
      startDate[1],
      startDate[2],
      startTime[0],
      startTime[1],
      startTime[2]
    );
    dataToSet.push([
      timeStamp,
      candle.Open,
      candle.High,
      candle.Low,
      candle.Close,
    ]);
  });
  return dataToSet;
};
