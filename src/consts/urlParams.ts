const IDENTIFIER = "AAPL.XNAS";
const IDENTIFIERTYPE = "Symbol";
const ADJUSTMENTMETHOD = "All";
const INCLUDEEXTENDED = "False";
const STARTTIME = "8/28/2020%2016:0";
const ENDTIME = "9/4/2020%2023:59";
const FIELDS =
  "ChartBars.StartDate,ChartBars.High,ChartBars.Low,ChartBars.StartTime,ChartBars.Open,ChartBars.Close,ChartBars.Volume";
const BASEURL = "https://www.fxempire.com/api/v1/en/stocks/chart/candles";
export {
  IDENTIFIER,
  IDENTIFIERTYPE,
  ADJUSTMENTMETHOD,
  INCLUDEEXTENDED,
  STARTTIME,
  ENDTIME,
  FIELDS,
  BASEURL,
};
